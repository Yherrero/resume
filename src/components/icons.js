export default {
  node: {
    icon: "fab fa-node-js",
    title: "Node.JS"
  },
  vue: {
    icon: "fab fa-vuejs",
    title: "Vue.JS"
  },
  rest: {
    icon: "fas fa-cog",
    title: "REST api"
  },
  react: {
    icon: "fab fa-react",
    title: "React.js"
  },
  html: {
    icon: "fab fa-html5",
    title: "HTML5"
  },
  js: {
    icon: "fab fa-js-square",
    title: "Vanilla Javascript"
  },
  css: {
    icon: "fab fa-css3-alt",
    title: "CSS3"
  },
  firebase: {
    icon: "fas fa-fire",
    title: "Firebase"
  },
  responsive: {
    icon: "fas fa-mobile-alt",
    title: "Responsive"
  },
  nuxt: {
    icon: "fas fa-server",
    title: "Nuxt.js - SSR"
  },
  gatsby: {
    icon: "fas fa-server",
    title: "Gatsby.js - SSR"
  },
  gatsbynuxt: {
    icon: "fas fa-server",
    title: "Gatsby.js + Nuxt.js - SSR"
  },
  contentful: {
    icon: "fab fa-cuttlefish",
    title: "Contentful - content manager"
  }
}