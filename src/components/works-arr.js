export default [
  {
    title: "JS.P - Client",
    desc: "Create and manage your events and share it to your friends ! - (Client)",
    link: "https://projet-iut-js.netlify.com/",
    date: "December 2018",
    tags: [
      "vue",
      "responsive"
    ]
  },
  {
    title: "JS.P - Server",
    desc: "Create and manage your events and share it to your friends ! - (Api)",
    link: "https://js-project-iut.herokuapp.com/api-docs/",
    date: "December 2018",
    tags: [
      "node",
      "rest"
    ]
  },
  {
    title: "AMANDINE ARCELLI",
    desc: "Personal website and portfolio - Amandine ARCELLI - Carver artist",
    link: "https://arcelli-vue.netlify.com/",
    date: "November 2018",
    tags: [
      "gatsbynuxt",
      "vue",
      "contentful",
      "responsive"
    ]
  },
  {
    title: "Scrappy",
    desc: "Online manga scan reader.",
    link: "https://scrapy.netlify.com/",
    date: "November 2018",
    tags: [
      "node",
      "rest",
      "vue",
      "responsive"
    ]
  },
  {
    title: "VUE - Todo",
    desc: "Simple personal TODO List.",
    link: "https://todo-y.netlify.com/",
    date: "October 2018",
    tags: [
      "vue",
      "firebase",
      "responsive"
    ]
  },
  {
    title: "2R - PK",
    desc: "Personal website and portfolio - ROMAIN 2R - Director/Editor",
    link: "https://2r-pk.netlify.com/",
    date: "September 2018",
    tags: [
      "react",
      "gatsby",
      "css",
      "responsive"
    ]
  }
]